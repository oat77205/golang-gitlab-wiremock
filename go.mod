module hl-mock-api

go 1.19

require (
	github.com/PaesslerAG/jsonpath v0.1.1
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/PaesslerAG/gval v1.2.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
