############################
# STEP 1 build executable binary
############################
FROM golang:1.19 AS builder

ARG CI_JOB_TOKEN
RUN git config --global url."https://oauth2:${CI_JOB_TOKEN}@gitlab.com/true-itsd".insteadOf "https://gitlab.com/true-itsd"
RUN go env -w GOPRIVATE="gitlab.com/true-itsd"
RUN go env -w GONOSUMDB="gitlab.com/true-itsd"

WORKDIR /go/src/app
COPY . .

RUN go install -v ./...

CMD ["hl-mock-api"]
