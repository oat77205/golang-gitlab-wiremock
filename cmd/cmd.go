package cmd

import (
	"hl-mock-api/pkg/api"
	"hl-mock-api/pkg/api/home"
	"hl-mock-api/pkg/api/wiremock"
	"hl-mock-api/pkg/config"
)

func Run(conf config.Config) {
	homeRoute := home.NewRouter(home.NewHandler(conf))
	wiremockUseCase := wiremock.NewUseCase()
	wiremockRoute := wiremock.NewRouter(wiremockUseCase)
	routers := api.NewRouters(homeRoute, wiremockRoute)
	apis := api.NewAPI(routers)
	apis.Register(conf)
}
