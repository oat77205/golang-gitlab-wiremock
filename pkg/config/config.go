package config

type Config struct {
	Port string
}

const (
	MockRequestPath  = "./mock/%s/request/%s"
	MockResponsePath = "./mock/%s/response/%s"
	MockRouteYmlPath = "./mock/%s/route.yml"
	MockPath         = "./mock"
)
