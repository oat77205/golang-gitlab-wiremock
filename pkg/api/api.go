package api

import (
	"fmt"
	"hl-mock-api/pkg/config"
	"hl-mock-api/pkg/status"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type API interface {
	Register(cfg config.Config)
}

type api struct {
	Router Routers
}

func (a *api) Register(cfg config.Config) {
	status.Banner()

	r := mux.NewRouter()
	headers := handlers.AllowedHeaders([]string{"*"})
	methods := handlers.AllowedMethods([]string{http.MethodGet, http.MethodPost, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodTrace, http.MethodDelete, http.MethodOptions})
	origins := handlers.AllowedOrigins([]string{"*"})

	// Router
	r.HandleFunc("/live", YourHandler).Methods("GET")
	r.HandleFunc("/ready", YourHandler).Methods("GET")

	a.Router.Initials(r)

	status.Started(cfg.Port)

	// Listening
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", cfg.Port), handlers.CORS(headers, methods, origins)(r)))
}

// NewAPI provide apis
func NewAPI(router Routers) API {
	return &api{
		Router: router,
	}
}

func YourHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("{}"))
}
