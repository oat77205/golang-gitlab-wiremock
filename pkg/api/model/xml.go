package model

type XmlOmx struct {
	Envelope struct {
		Soapenv string                 `json:"-soapenv"`
		Sub     string                 `json:"-sub"`
		Header  string                 `json:"Header"`
		Body    map[string]interface{} `json:"Body"`
	} `json:"Envelope"`
}
