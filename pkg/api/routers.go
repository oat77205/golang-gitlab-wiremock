package api

import (
	"hl-mock-api/pkg/api/home"
	"hl-mock-api/pkg/api/wiremock"

	"github.com/gorilla/mux"
)

type Routers interface {
	Initials(route *mux.Router)
}

type routers struct {
	HomeRoute     home.Router
	WiremockRoute wiremock.Router
}

func (r *routers) Initials(route *mux.Router) {
	r.HomeRoute.Initial(route)
	r.WiremockRoute.Initial(route)
}

func NewRouters(
	homeRoute home.Router,
	wiremockRoute wiremock.Router,
) Routers {
	return &routers{
		HomeRoute:     homeRoute,
		WiremockRoute: wiremockRoute,
	}
}
