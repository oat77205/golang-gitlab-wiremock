package main

import (
	"flag"
	"hl-mock-api/cmd"
	"hl-mock-api/pkg/config"
	"os"
)

func main() {
	port := flag.String("port", "8080", "a string port")
	flag.Parse()
	p := *port
	if os.Getenv("PORT") != "" {
		p = os.Getenv("PORT")
	}
	conf := config.Config{
		Port: p,
	}
	cmd.Run(conf)
}
